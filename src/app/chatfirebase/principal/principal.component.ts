import {Component, OnInit} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {Observable} from 'rxjs';
import {ChatService} from '../../shared/chat.service';

@Component({
  selector: 'app-chat-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent{

  constructor(public _cs: ChatService) {
  }
}
