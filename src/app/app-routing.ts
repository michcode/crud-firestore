import {RouterModule, Routes} from '@angular/router';
import {PrincipalComponent} from './chatfirebase/principal/principal.component';
import {EmployeesComponent} from './employees/employees.component';

const appRoutes: Routes = [
  {
    path: '',
    component: EmployeesComponent
  },
  {
    path: 'chats',
    component: PrincipalComponent
  }
];

export const routing = RouterModule.forRoot(appRoutes);
